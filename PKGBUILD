# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Daniel Eklöf <daniel at ekloef dot se>

pkgbase=foot
pkgname=("foot" "foot-terminfo")
pkgver=1.9.2
pkgrel=4
pkgdesc="Wayland terminal emulator - fast, lightweight and minimalistic"
arch=('x86_64' 'aarch64')
url=https://codeberg.org/dnkl/foot
license=(mit)
depends=('libxkbcommon' 'wayland' 'pixman' 'fontconfig' 'libutf8proc' 'fcft')
makedepends=('meson' 'ninja' 'scdoc' 'python' 'ncurses' 'wayland-protocols' 'tllist')  # ‘llvm’, for PGO with clang
checkdepends=('check')
source=(${pkgname}-${pkgver}.tar.gz::${url}/archive/${pkgver}.tar.gz)
sha256sums=('5859d8d7293da4c7c52f45a9756fc7989edc364057e8b86b4e1fbea7bc2b4009')

build() {
  cd foot

  PGO=auto
  ./pgo/pgo.sh ${PGO} . build --prefix=/usr --wrap-mode=nodownload -Dterminfo=disabled
  
  # build terminfo files
  echo "Building terminfo files for seperate package..."
  sed 's/@default_terminfo@/foot-extra/g' foot.info | tic -x -o build -e foot-extra,foot-extra-direct -
}

package_foot() {
  pkgdesc="Wayland terminal emulator - fast, lightweight and minimalistic"
  depends+=('foot-terminfo')
  optdepends=('libnotify: desktop notifications'
              'xdg-utils: URI launching'
              'bash-completion: bash completions for foot itself')

  cd foot
  DESTDIR="${pkgdir}/" ninja -C build install
  rm -rf "${pkgdir}/usr/share/terminfo"
  install -Dm 644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

package_foot-terminfo() {
  pkgdesc="Alternative terminfo files for the foot terminal emulator, with additional non-standard capabilities"
  depends+=('ncurses')
  install=$pkgname.install

  cd foot
  install -dm 755 "${pkgdir}/usr/share/terminfo/f"
  cp build/f/foot-extra{,-direct} "${pkgdir}/usr/share/terminfo/f/"

}
